package com.fra.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fra.csv.ApachePOICSVGenerate;
import com.fra.util.ExcelToCSV;

import Excel.GeneratoreExcel;

@RestController

public class Controller {

	public List<String>sheet=new ArrayList<>();
	XSSFWorkbook workbook = new XSSFWorkbook();
	
	@SuppressWarnings("unchecked")
	@PostMapping("/generate")
	public String converter(@RequestBody JSONObject o) throws Exception {
		
		/* Parte uno interrogazione ndg e prefix  */
		String file="C:/Users/Francesco Marchitell/ESERCITAZIONI_BNL/Excel-CSV/tmp/"+o.get("ndg")+"-"+o.get("prefix")+".xlsx";
		String csv="C:/Users/Francesco Marchitell/ESERCITAZIONI_BNL/Excel-CSV/tmp/"+o.get("ndg")+"-"+o.get("prefix")+".csv"; 
		
		/*Parte due interrogazione section per scegliere il servizio*/
		String section=(String) o.get("section");
		switch(section) {
			case "ANA": /* a questo punto ci sarà la chiamata al servizio corrispondente che ci restituirà l'oggetto java corrispondente  */
				break;
			case "PRO":
				break;
			case "INF":
				break;
		}
		
		/*Parte tre interrogazione type per scegliere il tipo di file da generare*/
		String type=(String) o.get("type");
		switch(type) {
		case "EXCEL":
			
			List<String> sheetStringhe=(List<String>) o.get("sheetList");
			List<String> campiColonne;
			for(String x : sheetStringhe) {
				campiColonne=(List<String>) o.get(x);
				XSSFSheet sheetX = workbook.createSheet(x);
				GeneratoreExcel.generateExcel(sheetX, campiColonne);
			}
				try {
			        FileOutputStream outputStream = new FileOutputStream(file);
			        workbook.write(outputStream);
			        workbook.close();
			    } catch (FileNotFoundException e) {
			        e.printStackTrace();
			    } catch (IOException e) {
			        e.printStackTrace();
			    }

			    System.out.println("Done");
			
			//break;
			    
			    
		case "CSV":	
			
			ExcelToCSV.excelToCSV(file, csv);
			break;	
		}
		
		
		return null;
	}

}






////PROVE CSV

//String prefix=(String) o.get("prefix");  //ci servirà per chiamare il servizio
//List<XSSFSheet> xxx= new ArrayList<>();	 //conterrà gli sheet se eventualmente serviranno per andare a capo
//sheet=(List<String>) o.get("sheetList"); //lista sheet
//List<String> list = new ArrayList();	 //lista di stringhe da passare al metodo per creare il csv
//for(String sheetX:sheet) {
//	//list.add(sheetX);
//	//ApachePOICSVGenerate.generateCSV(list,xxx);
//	//passare il contenuto di ogni sheet!!!!!!!!!!
//	List<Object> sheetType =(List<Object>) o.get("anagraficaDiBaseSh");
//		for(Object singleJson: sheetType) {
//			System.out.println(singleJson);
//		}
//	}