package com.fra.OutputBean;

import java.util.List;

import com.fra.ANA.BnlData;
import com.fra.ANA.Document;
import com.fra.ANA.Family;
import com.fra.ANA.HistoricalContact;
import com.fra.ANA.Registry;

public class OutputBean {

	public List<BnlData> bnlData;
	public List<Document> document;
	public List<Family> family;
	public List<HistoricalContact> hc;
	public List<Registry> registry;
}
