package Excel;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcel {

public static void createExcel(List<String> colonne,Sheet s) {
    	
//	    XSSFWorkbook workbook = new XSSFWorkbook();
	    System.out.println(s.getSheetName());
	    
	    int colNum = 0;
	    int rowNum = 0;
	  
	    Row row = s.createRow(rowNum++);
	    
	    for (Object column : colonne) {
	            Cell cell = row.createCell(colNum++);
	            if (column instanceof String) {
	                cell.setCellValue((String)column);
	            } 
	                else if (column instanceof Integer) {
	                cell.setCellValue((Integer) column);
	            }
	        }   
	    }
}
